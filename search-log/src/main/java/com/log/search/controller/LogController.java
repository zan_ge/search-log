package com.log.search.controller;

import com.log.search.dto.FileDto;
import com.log.search.dto.LogInfoDto;
import com.log.search.dto.LogSearchDto;
import com.log.search.dto.ViewResult;
import com.log.search.service.LogService;
import com.log.search.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping("/search")
    public ViewResult<List<LogInfoDto>>  search(@RequestBody LogSearchDto searchDto){
        if(CommonUtils.valiErrorStr(searchDto.getKeyword())){
            return ViewResult.failed("非法操作");
        }
        if(CommonUtils.valiErrorStr(searchDto.getFileName())){
            return ViewResult.failed("非法操作");
        }
        if(CommonUtils.valiErrorStr(searchDto.getUpLine())){
            return ViewResult.failed("非法操作");
        }
        ViewResult<List<LogInfoDto>> viewResult = ViewResult.success(logService.searchLog(searchDto));
        return viewResult;

    }
    @PostMapping("/allSearch")
    public ViewResult<List<LogInfoDto>>  allSearch(@RequestBody LogSearchDto searchDto){

        if(CommonUtils.valiErrorStr(searchDto.getFileName())){
            return ViewResult.failed("非法操作");
        }
        ViewResult<List<LogInfoDto>> viewResult = ViewResult.success(logService.allSearch(searchDto));
        return viewResult;

    }
    @GetMapping("/searchFileName")
    public ViewResult<List<FileDto>>  searchFile(@RequestParam long appId,String folder){
        if(CommonUtils.valiErrorStr(folder)){
            return ViewResult.failed("非法操作");
        }
        return ViewResult.success(logService.searchFileName(appId,folder));

    }

}
